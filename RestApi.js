function comingMovies() {
    fetch('http://localhost:3000/movies-coming')
        .then(response => response.json())
        .then(value => {
            var parent = document.getElementById('movies');
            parent.innerHTML='';
            for (var val of value) {
                var c1 = document.createElement('div');
                var ele = document.createElement('p');
                var text = document.createTextNode(`Title : ${val.title}` + ` ` + `${val.imdbRating}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('span');
                var text = document.createTextNode(`Genres : ${val.genres}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('p');
                var text = document.createTextNode(`Year : ${val.year}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('img');
                ele.setAttribute('src', `img/${val.poster}`);
                c1.appendChild(ele);
                var ele= document.createElement('button');
                ele.className='fav-btn';
                ele.onclick=function(){
                    fav(this.id)
                };
                var text = document.createTextNode('favourit');
                ele.appendChild(text);
                c1.appendChild(ele);
                parent.appendChild(c1);
            }
        });
}
comingMovies();




function theaterMovies() {
    fetch('http://localhost:3000/movies-in-theaters')
        .then(response => response.json())
        .then(value => {
            var parent = document.getElementById('movies');
            parent.innerHTML='';
            for (var val of value) {
                var c1 = document.createElement('div');
                var ele = document.createElement('p');
                var text = document.createTextNode(`Title : ${val.title}` + ` ` + `${val.imdbRating}Imdb`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('span');
                var text = document.createTextNode(`Genres : ${val.genres}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('p');
                var text = document.createTextNode(`Year : ${val.year}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('img');
                ele.setAttribute('src', `img/${val.poster}`);
                c1.appendChild(ele);
                var ele= document.createElement('button');
                ele.className='fav-btn';
                ele.onclick=function(){
                    fav(this.id)
                };
                var text = document.createTextNode('favourit');
                ele.appendChild(text);
                c1.appendChild(ele);
                parent.appendChild(c1);
            }
        });
}
theaterMovies();



function TopRatedIndia() {
    fetch('http://localhost:3000/top-rated-india')
        .then(response => response.json())
        .then(value => {
            var parent = document.getElementById('movies');
            parent.innerHTML='';
            for (var val of value) {
                var c1 = document.createElement('div');
                var ele = document.createElement('p');
                var text = document.createTextNode(`Title : ${val.title}` + ` ` + `${val.imdbRating}Imdb`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('span');
                var text = document.createTextNode(`Genres : ${val.genres}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('p');
                var text = document.createTextNode(`Year : ${val.year}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('img');
                ele.setAttribute('src', `img/${val.poster}`);
                c1.appendChild(ele);
                var ele= document.createElement('button');
                ele.className='fav-btn';
                ele.onclick=function(){
                    fav(this.id)
                };
                var text = document.createTextNode('favourit');
                ele.appendChild(text);
                c1.appendChild(ele);
                parent.appendChild(c1);
            }
        });
}
TopRatedIndia();

function topRatedMovies() {
    fetch('http://localhost:3000/top-rated-movies')
        .then(response => response.json())
        .then(value => {
            var parent = document.getElementById('movies');
            parent.innerHTML='';
            for (var val of value) {
                var c1 = document.createElement('div');
                var ele = document.createElement('p');
                var text = document.createTextNode(`Title : ${val.title}` + ` ` + `${val.imdbRating}Imdb`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('span');
                var text = document.createTextNode(`Genres : ${val.genres}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('p');
                var text = document.createTextNode(`Year : ${val.year}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('img');
                ele.setAttribute('src', `img/${val.poster}`);
                c1.appendChild(ele);
                var ele= document.createElement('button');
                ele.className='fav-btn';
                ele.onclick=function(){
                    fav(this.id)
                };
                var text = document.createTextNode('favourit');
                ele.appendChild(text);
                c1.appendChild(ele);
                parent.appendChild(c1);
            }
        });
}
topRatedMovies();


     function fav() {
        fetch('http://localhost:3000/favourit')
         .then(response => response.json())
         .then(res => console.log(res))
          .then(value => {
            var parent = document.getElementById('movies');
            parent.innerHTML='';
                for(var val of value){
                var c1 = document.createElement('div');
                var ele = document.createElement('p');
                var text = document.createTextNode(`Title : ${val.title}` + ` ` + `${val.imdbRating}Imdb`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('span');
                var text = document.createTextNode(`Genres : ${val.genres}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('p');
                var text = document.createTextNode(`Year : ${val.year}`);
                ele.appendChild(text);
                c1.appendChild(ele);
                var ele = document.createElement('img');
                ele.setAttribute('src', `img/${val.poster}`);
                c1.appendChild(ele);
                var ele= document.createElement('button');
                ele.className='fav-btn';
                ele.onclick=function(){
                    fav(this.id)
                };
                var text = document.createTextNode('favourit');
                ele.appendChild(text);
                c1.appendChild(ele);
                parent.appendChild(c1);
            }
});
}
async function remove(a)
{
try{
    let res=await fetch(`http://localhost:3000/favourit/${id}`,{
        method: "DELETE",
    });
}
catch(err){}

}
fav();